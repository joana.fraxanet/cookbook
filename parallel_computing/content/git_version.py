import os
import subprocess

def check_git_commit(path):
    path = os.path.expanduser(path)
    folder = os.path.dirname(path)

    def check_version(folder):
        ver = subprocess.check_output(['git', 'rev-parse', 'HEAD'], cwd=folder)
        return ver.split("\n")[0]

    def check_branch(folder):
        cmds = ['git', 'rev-parse', '--abbrev-ref', 'HEAD']
        branch = subprocess.check_output(cmds, cwd=folder)
        return branch.split("\n")[0]

    def check_status(folder):
        branch = check_branch(folder)
        if subprocess.check_output(['git', 'diff'], cwd=folder):
            return branch + '_modified'
        else:
            return branch + '_clean'

    version = check_version(folder)
    status = check_status(folder)
    machine = subprocess.check_output(['uname', '-n']).split("\n")[0]

    return {'machine': machine, 'commit': version, 'branch_status': status}
