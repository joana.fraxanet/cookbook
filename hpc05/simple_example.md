# Simple parallel example with `hpc05`.

Run this code in a notebook.

```python
import hpc05
client, dview, lview = hpc05.start_remote_and_connect(100, profile='pbs2', folder='~/the_folder_you_are_running_the_code_from_on_hpc05')
```

```python
import numpy as np
x = np.linspace(-2, 2)
y = lview.map_async(np.sin, x)
y.wait_interactive()
y = y.get()
```

# See your result
```python
from matplotlib import pyplot as plt
%matplotlib inline
plt.plot(x,y)
```

# Kill engines

```python
hpc05.kill_remote_ipcluster()  # or `client.shutdown(hub=True)`
```