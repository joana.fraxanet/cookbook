# How to set up Gitlab CI to upload files to Dropbox #

1. First you need to generate a deploy key for Dropbox.
   For this, you go to Dropbox apps page
    https://www.dropbox.com/developers/apps
   and create a new app with "Full Dropbox" permissions.
   After you finish it, you will arrive to a configuration page of this app.
   You will need to configure a new access token.

1. After that you need to put this token to secret variables of your CI/CD in
   Gitlab. For that you go to settings of your project -> CI/CD -> Secret
   variables -> Add a variable. You should add a variable called
   `DROPBOX_DEPLOY_TOKEN` with a token value generated in previous step.

1. For deploy itself we will use a Bash script from here:
    https://github.com/andreafabrizi/Dropbox-Uploader
   Suppose our repo contains file `example.tex`, which will be compiled to PDF,
   using command `latexmk -pdf example.tex`. Then we want to upload it to our
   Dropbox folder under the path `/nanoscience/projectname/article` together
   with its LaTeX source, but only in the case we push to our master branch.
   To do it, we need to create a file called `.gitlab-ci.yml` in the root of
   your repository and place there following content:
    ```yaml
    stages:
      - build
      - deploy

    build:
      stage: build
      script:
        - latexmk -pdf example.tex
      artifacts:
        when: on_success
        paths:
        - example.pdf

    deploy_dropbox:
      stage: deploy
      only:
        - master@nano/reponame
      dependencies:
        - build
      variables:
        TARGET_FOLDER: "/nanoscience/projectname/article"
        UPLOADER_FETCH_URL: "https://raw.githubusercontent.com/andreafabrizi/Dropbox-Uploader/master/dropbox_uploader.sh"
        UPLOADER: "dropbox_uploader.sh"
        UPLOADER_CONFIG: "dropbox_uploader.cfg"
      before_script:
        - curl "${UPLOADER_FETCH_URL}" -o "${UPLOADER}"
        - echo "OAUTH_ACCESS_TOKEN=${DROPBOX_DEPLOY_TOKEN}" > "${UPLOADER_CONFIG}"
      script:
        - bash "${UPLOADER}" -f "${UPLOADER_CONFIG}" mkdir "${TARGET_FOLDER}" &>/dev/null || echo "Target folder exists"
        - bash "${UPLOADER}" -f "${UPLOADER_CONFIG}" upload example.pdf "${TARGET_FOLDER}"
        - bash "${UPLOADER}" -f "${UPLOADER_CONFIG}" upload example.tex "${TARGET_FOLDER}"
      after_script:
        - rm -f "${UPLOADER}"
        - rm -f "${UPLOADER_CONFIG}"
    ```
   Here we configure building of files in `build` job in `script` clause,
   and copying to Dropbox -- in `deploy_dropbox` job.
   After build is executed, produced files are accessible via the URL like:
    https://gitlab.kwant-project.org/USER_OR_GROUP/PROJECT_NAME/builds/artifacts/master/file/example.pdf?job=build
   In `script` clause of `deploy_dropbox` section we set up an upload command
   for every file we want.
   Alternatively, we may specify them using wildcards, for example
   ```
   bash "${UPLOADER}" -f "${UPLOADER_CONFIG}" upload *.pdf "${TARGET_FOLDER}"
   ```
   would upload all PDF files from current folder, and
   ```
   bash "${UPLOADER}" -f "${UPLOADER_CONFIG}" upload example.* "${TARGET_FOLDER}"
   ```
   would upload all files with a name, that starts with `example.`.
   This will be invoked only for master branch of your repository (that is
   set up by `only` clause of `deploy_dropbox` job, put there correct
   repository name).
   For more advanced usage, you may read [official documentation](https://docs.gitlab.com/ce/ci/yaml/README.html).
   For validation of your `.gitlab-ci.yml` you may use Gitlab built-in tool
   [Lint](https://gitlab.kwant-project.org/ci/lint).
1. The last step is a configuring runners for a project. It is done in Project's
   Settings -> CI/CD -> Runners settings. You can either use Shared Runners, or
   set up your own, this is out of scope of this guide.
