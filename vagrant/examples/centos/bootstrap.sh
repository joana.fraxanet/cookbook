#!/usr/bin/env bash
echo 'export PS1="\[\e[1;34m\]\u@vm-centos:\W> \[\e[0m\]"' > /home/vagrant/.bash_profile
timedatectl set-timezone Europe/Amsterdam
