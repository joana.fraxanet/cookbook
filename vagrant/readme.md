# First get VirtualBox (Mint 18, Ubuntu 16.04, for older check webpage)
```
sudo apt-add-repository "deb http://download.virtualbox.org/virtualbox/debian xenial contrib"
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
sudo apt-get update
sudo apt-get install virtualbox-5.1 virtualbox-dkms dkms
```


# Installation via the command line (Ubuntu)
```
wget https://releases.hashicorp.com/vagrant/1.8.6/vagrant_1.8.6_x86_64.deb
echo "e6d83b6b43ad16475cb5cfcabe7dc798002147c1d048a7b6178032084c7070da vagrant_1.8.6_x86_64.deb" | sha256sum -c -
sudo dpkg -i vagrant_1.8.6_x86_64.deb
rm vagrant_1.8.6_x86_64.deb
```


# Test if it works
```
vagrant init hashicorp/precise64
vagrant up
vagrant ssh
```

# More examples
just ``cd`` into folder with an example and do ``vagrant up``.

One can make ssh-ing into them more easy (for example testing playbooks) with
including output of ``vagrant ssh-config`` in your ``~/.ssh/config``.


# Other useful tricks
## Destroying all vagrant machines
```
vagrant global-status | grep virtual | awk '{print $1;}' | xargs -I % vagrant destroy -f %
```

## Cleaning vms after removing .vagrant file
```
vagrant global-status
vboxmanage list vms

VBoxManage list runningvms | grep vagrant | awk '{print $2;}' | xargs -I vmid VBoxManage controlvm vmid poweroff
VBoxManage list vms | grep vagrant | awk '{print $2;}' | xargs -I vmid VBoxManage unregistervm vmid
```
and remove corresponding folder with ``vdi``.
