Cool Git Things
---------------
Here is a collection of git commands/options that I thought were pretty cool.
All of these were taken directly from the [Git Book](https://git-scm.com/book/en/v2)
which I *highly* recommend reading through in order to supercharge your git usage.

## Poor man's gitk
You can get a pretty log of the repo history with
~~~
git log --all --oneline --decorate --graph
~~~
You can also use `watch` to have an auto-updating view of the repository:
~~~
watch --color git log --all --oneline --decorate --graph --color
~~~
This is useful to keep in a terminal window as you are working to
see what is going on with your repository. You can also use
~~~
git status -s
~~~
to get a shortened status of the working directory compared to the
staging area, and use `watch` in a similar way.

### Richer man's gitk
The program `tig` is like `gitk`, but runs in a terminal, which makes it ideal
for visualising repositories on servers with no X installed. It can be used
to browse the repo history, and you can set up keybindings to run custom
commands from within `tig` itself (e.g. checkout the currently selected commit,
cherry-pick, fetch, ...).

## Git aliases
It is annoying to have to write out all those options every time. Luckily, git
lets you define *aliases* that you can use like regular git command. If I first
set up an alias `nice-log` by saying:
~~~
git config --global alias.nice-log 'log --all --oneline --decorate --graph'
~~~
then I can just say:
~~~
git nice-log
~~~
to get my pretty repo history graph!

## Searching the repository
You can search for a regular expression in all the files tracked by git with
~~~
git grep <regexp>
~~~
If you want to look through **the history of the changes to a bunch of lines
in a file** you can use
~~~
git log -L <start>,<stop>:<file name>
~~~
to look at a log of the changes made to lines between `start` and `stop` in `file`.

## Separating out your changes
Try and make your commits into logical units. This is often quite difficult, and you
will tend to add lots of little unrelated changes. You can use
~~~
git add -p file
~~~
to select which chunks of changes you want to add to the staging area.
You can also do an interactive `add` with
~~~
git add -i
~~~

## Checking whitespace
Use
~~~
git diff --check
~~~
to check for whitespace errors before commit

## Contributing to projects
Please, everyone, read the [Git Book](https://git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project)
for guidelines on how to contribute projects (e.g. Kwant).

## Working on several branches
Say you are working on a branch (with uncommitted changes) and quickly need to check something else
out in another branch (e.g. to apply a quick bugfix). You could make a "WIP" commit
and then `git checkout` the other branch, but this is ugly and pollutes the repo
history.

### 1st alternative
Use
~~~
git stash
~~~
to "stash" all the changes, then you can `git checkout` the other branch and do
whatever work you have to do. Afterwards, you can `git checkout` your original
branch and get back your uncommitted changes with
~~~
git stash pop
~~~

### 2nd alternative
A little-known feature of git is that you can actually have *multiple working
trees* checked out at the same time. For example, if you want to checkout
branch `br` in a directory `br_dir` then all we have to do is
~~~
git worktree add br_dir br
~~~
and we can `cd br_dir` and start hacking away! We can list the currently
active working trees with
~~~
git worktree list
~~~
Once we are done with a subtree we can remove it with
~~~
rm -r br_dir  # remove the worktree directory
git worktree --prune  # remove worktree metadata from git
~~~

## Purging files from a git repository
First, **be careful**; this can completely nuke your whole repository, so
tread lightly.

You can remove the file `passwords.txt` from the **entire history** of your
repository by saying
~~~
git filter-branch --tree-filter 'rm -rf passwords.txt'
~~~

In general `filter-branch` will let you walk the whole history (starting from a
given commit; the current branch by default) and apply a filter to various
aspects of the commits (working tree, index, commit message, etc.). This could
be used to, for example, to retroactively alter your committer email or name.

Because you are rewriting history, do not use this technique if you have
already shared your changes.

Actually, do not use this technique at all. Forget I mentioned it. Seriously.

## Nicer diffs

If you're using a sufficiently recent git, you can let it try harder to find
nicer looking diffs by enabling these options:
~~~
git config --global diff.indentHeuristic true
git config --global diff.compactionHeuristic true
~~~