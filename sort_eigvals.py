import collections
from scipy.optimize import linear_sum_assignment
import numpy as np

def best_match(psi1, psi2, threshold=None):
    """Find the best match of two sets of eigenvectors.

    Parameters
    ----------
    psi1, psi2 : numpy 2D complex arrays
        Arrays of initial and final eigenvectors.
    threshold : float, optional
        Minimal overlap when the eigenvectors are considered belonging to
        the same band. The default value is :math:`1/sqrt(2N)`,
        where :math:`N` is the length of each eigenvector.

    Returns
    -------
    sorting : numpy 1D integer array
        Permutation to apply to ``psi2`` to make the optimal match.
    diconnects : numpy 1D bool array
        The levels with overlap below the ``threshold`` that should be
        considered disconnected.
    """
    if threshold is None:
        threshold = (2 * psi1.shape[1])**-0.5
    Q = np.abs(psi1.T.conj() @ psi2)  # Overlap matrix
    orig, perm = linear_sum_assignment(-Q)
    return perm, Q[orig, perm] < threshold


def get_sorted_spectrum(eigensystem, xvals):
    e, psi = eigensystem(xvals[0])
    sorted_levels = [e]
    swaps = collections.defaultdict(list)
    N = 0
    for i, x in enumerate(xvals[1:]):
        e2, psi2 = eigensystem(x)
        perm, line_breaks = best_match(psi, psi2)
        e2 = e2[perm]
        for j in np.argwhere(line_breaks).reshape(-1):
            swaps[j].append(i+1)
            N += 1
        psi = psi2[:, perm]
        e = e2
        sorted_levels.append(e)

    # Some eigvals had no matching eigvecs to swap with, these
    # are in `swaps`.
    # For example we have this discontineous matrix with jumps in two places:
    # sorted_levels = [0, 0, 1, 1, 0, 0]
    # So we have three bands, therefore extend we `sorted_levels`
    # with two rows of NaNs.
    # We get:
    # sorted_levels = [[0, 0, 1, 1, 0, 0],
    #                  [nan, nan, nan, nan, nan, nan],
    #                  [nan, nan, nan, nan, nan, nan]]
    # we want to change this to
    # sorted_levels = [[0, 0, nan, nan, nan, nan],
    #                  [nan, nan, 1, 1, nan, nan],
    #                  [nan, nan, nan, nan, 0, 0]]

    sorted_levels = np.pad(sorted_levels, ((0, 0), (0, N)), mode='constant',
                           constant_values=(np.nan, np.nan))

    swap_to = sorted_levels.shape[1] - N
    for swap_from, xs in swaps.items():
        xs = xs + [None]
        for j in range(len(xs) - 1):
            row = sorted_levels[xs[j]:xs[j+1]]
            A, B = row[:, swap_from].copy(), row[:, swap_to].copy()
            row[:, swap_from], row[:, swap_to] = B, A
            swap_to += 1

    return sorted_levels


# Example:
def upd(d, **kwargs):
    for k, v in kwargs.items():
        d[k] = v
    return d

syst = make_syst()
eigensystem = (lambda phi: funcs.sparse_diag(syst.hamiltonian_submatrix(
                 params=upd(params, phi=phi), sparse=True), k=20, sigma=0))
phis = np.linspace(0, 2 * np.pi, 21)
energies_sorted = get_sorted_spectrum(eigensystem, phis)