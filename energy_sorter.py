import numpy as np

def energy_sorter(e_array, wf_array):
    """ Sort energies and wave funtions

    Sort the arrays of energies contiguously, following the highest
    projection of the previous wave function on the new wave functions.

    Parameters
    ----------
    e_array : numpy ndarray
        Contains the arrays or energies.
    wf_array : numpy ndarray
        The wave functions corresponding to the energies in `e_array`.

    Returns
    -------
    sort : array if ints
        Indices of the sorted array.
    """
    num_vectors = len(e_array)
    assert len(wf_array) == num_vectors
    dim = len(wf_array[0])
    sort = np.ndarray(shape=(num_vectors, dim), dtype=int)
    sort[0] = np.arange(dim)
    for i_vec in range(1, num_vectors):
        wf0c =  np.transpose(np.conjugate(wf_array[i_vec - 1]))[sort[i_vec-1]]
        e = e_array[i_vec]
        wf = wf_array[i_vec]
        # proyect the wf to the previous wf
        proj = np.abs(np.dot(wf0c, wf[:]))
        # pick best wf pairs first, ensuring bijectivity
        found_cols = []
        found_rows = []
        loop = 0
        while ((len(found_cols) < dim or len(found_rows) < dim)
               and loop < dim ** 2):
            loop += 1
            argmax = np.argmax(proj)
            col = argmax // dim
            row = argmax - dim * (argmax // dim)
            proj[col, row] *= 0.
            if col not in found_cols and row not in found_rows:
                sort[i_vec, col] = row
                found_cols.append(col)
                found_rows.append(row)
    return sort
