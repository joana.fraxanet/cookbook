"""
Script to logs the progress of a ipyparallel.Client().load_balanced_view() map.
"""
# Copyright (c) Bas Nijholt
# Distributed under the terms of the Modified BSD License

def progress_bar(ar, *, update_interval=0.1, blocking=False):
    from tqdm import tqdm_notebook
    pbar = tqdm_notebook(total=len(ar.msg_ids), smoothing=0)
    if blocking:
        import time
        val = 0
        while not ar.ready():
            new_val = ar.progress
            pbar.update(new_val - val)
            val = new_val
            time.sleep(update_interval)
        pbar.update(pbar.total - pbar.n)
    else:
        import asyncio
        async def update():
            val = 0
            while not ar.ready():
                new_val = ar.progress
                pbar.update(new_val - val)
                val = new_val
                await asyncio.sleep(update_interval)
            pbar.update(pbar.total - pbar.n)

        asyncio.get_event_loop().create_task(update())

if __name__ == '__main__':
    # Useage example
    from ipyparallel import Client
    from time import sleep
    c = Client()
    lview = c.load_balanced_view()
    ar = lview.map_async(sleep, 1000 * [1])

    progress_bar(ar)
